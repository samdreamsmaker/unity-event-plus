﻿using UnityEngine;
using UnityEngine.Events;

namespace Utilities.EventManagers {
    abstract class UnityEventManager : MonoBehaviour {
        [SerializeField]
        UnityEvent unityEvent;
        public UnityEvent GetUnityEvent() {
            return unityEvent;
        }
        public void SetUnityEvent(UnityEvent unityEvent) {
            this.unityEvent = unityEvent;
        }
        protected void InvokeEvent() {
            unityEvent.Invoke();
        }
    }
}
