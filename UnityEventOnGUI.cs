﻿using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventOnGUI : UnityEventManager {
        void OnGUI() {
            InvokeEvent();
        }
    }
}