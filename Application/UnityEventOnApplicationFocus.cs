﻿using UnityEngine;

namespace Utilities.EventManagers.Application {
    class UnityEventOnApplicationFocus : UnityEventManager {
        void OnApplicationFocus(bool hasFocus) {
            InvokeEvent();
        }
    }
}