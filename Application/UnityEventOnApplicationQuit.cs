﻿using UnityEngine;

namespace Utilities.EventManagers.Application {
    class UnityEventOnApplicationQuit : UnityEventManager {
        void OnApplicationQuit() {
            InvokeEvent();
        }
    }
}