﻿using UnityEngine;

namespace Utilities.EventManagers.Application {
    class UnityEventOnApplicationPause : UnityEventManager {
        void OnApplicationPause(bool pauseStatus) {
            InvokeEvent();
        }
    }
}