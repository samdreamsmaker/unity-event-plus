﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utilities.EventManagers {
    class UnityEventActiveSceneChanged : UnityEventManager {
        //[SerializeField]
        bool conditional = true;
        //[SerializeField]
        int activatorSceneId = 1;

        bool canInvokeEvent = true;

        void Start() {
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }
        void OnActiveSceneChanged(Scene previousScene, Scene newScene) {
            if ((conditional && canInvokeEvent) || !conditional) { InvokeEvent(); }
            UpdateActivation(newScene);
        }
        void UpdateActivation(Scene newScene) {
            if (newScene.buildIndex.Equals(activatorSceneId)) canInvokeEvent = true;
            else canInvokeEvent = false;
        }
    }
}
