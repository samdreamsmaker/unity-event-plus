﻿using UnityEngine;

namespace Utilities.EventManagers.Particle {
    class UnityEventOnParticleSystemStopped : UnityEventManager {
        void OnParticleSystemStopped() {
            InvokeEvent();
        }
    }
}