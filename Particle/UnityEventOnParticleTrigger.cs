﻿using UnityEngine;

namespace Utilities.EventManagers.Particle {
    class UnityEventOnParticleTrigger : UnityEventManager {
        void OnParticleTrigger() {
            InvokeEvent();
        }
    }
}