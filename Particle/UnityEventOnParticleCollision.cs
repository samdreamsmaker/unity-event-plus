﻿using UnityEngine;

namespace Utilities.EventManagers.Particle {
    class UnityEventOnParticleCollision : UnityEventManager {
        void OnParticleCollision(GameObject other) {
            InvokeEvent();
        }
    }
}