﻿using UnityEngine;

namespace Utilities.EventManagers.Server {
    class UnityEventOnServerInitialized : UnityEventManager {
        void OnServerInitialized() {
            InvokeEvent();
        }
    }
}