﻿using UnityEngine;

namespace Utilities.EventManagers.Server {
    class UnityEventOnConnectedToServer : UnityEventManager {
        void OnConnectedToServer() {
            InvokeEvent();
        }
    }
}