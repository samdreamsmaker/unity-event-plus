﻿using UnityEngine;

namespace Utilities.EventManagers.Visibility {
    class UnityEventOnBecameInvisible : UnityEventManager {
        void OnBecameInvisible() {
            InvokeEvent();
        }
    }
}