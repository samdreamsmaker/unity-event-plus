﻿using UnityEngine;

namespace Utilities.EventManagers.Visibility {
    class UnityEventOnBecameVisible : UnityEventManager {
        void OnBecameVisible() {
            InvokeEvent();
        }
    }
}