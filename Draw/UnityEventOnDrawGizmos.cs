﻿using UnityEngine;

namespace Utilities.EventManagers.Draw {
    class UnityEventOnDrawGizmos : UnityEventManager {
        void OnDrawGizmos() {
            InvokeEvent();
        }
    }
}