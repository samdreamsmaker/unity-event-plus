﻿using UnityEngine;

namespace Utilities.EventManagers.Draw {
    class UnityEventOnDrawGizmosSelected : UnityEventManager {
        void OnDrawGizmosSelected() {
            InvokeEvent();
        }
    }
}