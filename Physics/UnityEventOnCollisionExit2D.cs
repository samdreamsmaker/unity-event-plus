﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionExit2D : UnityEventManager {
        void OnCollisionExit2D(Collision2D collision) {
            InvokeEvent();
        }
    }
}