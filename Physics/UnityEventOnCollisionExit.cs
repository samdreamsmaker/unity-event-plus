﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionExit : UnityEventManager {
        void OnCollisionExit(Collision collision) {
            InvokeEvent();
        }
    }
}