﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionStay : UnityEventManager {
        void OnCollisionStay(Collision collision) {
            InvokeEvent();
        }
    }
}