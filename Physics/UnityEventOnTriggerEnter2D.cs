﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerEnter2D : UnityEventManager {
        void OnTriggerEnter2D(Collider2D other) {
            InvokeEvent();
        }
    }
}