﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerExit : UnityEventManager {
        void OnTriggerExit(Collider other) {
            InvokeEvent();
        }
    }
}