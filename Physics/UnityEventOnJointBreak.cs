﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnJointBreak : UnityEventManager {
        void OnJointBreak(float breakForce) {
            InvokeEvent();
        }
    }
}