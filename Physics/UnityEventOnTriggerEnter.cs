﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerEnter : UnityEventManager {
        void OnTriggerEnter(Collider other) {
            InvokeEvent();
        }
    }
}