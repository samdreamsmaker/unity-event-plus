﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerStay : UnityEventManager {
        void OnTriggerStay(Collider other) {
            InvokeEvent();
        }
    }
}