﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerExit2D : UnityEventManager {
        void OnTriggerExit2D(Collider2D other) {
            InvokeEvent();
        }
    }
}