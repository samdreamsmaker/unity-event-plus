﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionEnter2D : UnityEventManager {
        void OnCollisionEnter2D(Collision2D collision) {
            InvokeEvent();
        }
    }
}