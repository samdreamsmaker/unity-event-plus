﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnTriggerStay2D : UnityEventManager {
        void OnTriggerStay2D(Collider2D other) {
            InvokeEvent();
        }
    }
}