﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionStay2D : UnityEventManager {
        void OnCollisionStay2D(Collision2D collision) {
            InvokeEvent();
        }
    }
}