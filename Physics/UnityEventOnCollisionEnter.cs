﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnCollisionEnter : UnityEventManager {
        void OnCollisionEnter(Collision collision) {
            InvokeEvent();
        }
    }
}