﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnJointBreak2D : UnityEventManager {
        void OnJointBreak2D(Joint2D brokenJoint) {
            InvokeEvent();
        }
    }
}