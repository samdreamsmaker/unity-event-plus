﻿using UnityEngine;

namespace Utilities.EventManagers.Physics {
    class UnityEventOnControllerColliderHit : UnityEventManager {
        void OnControllerColliderHit(ControllerColliderHit hit) {
            InvokeEvent();
        }
    }
}