﻿using UnityEngine;

namespace Utilities.EventManagers.Mouse {
    class UnityEventOnMouseOver : UnityEventManager {
        void OnMouseOver() {
            InvokeEvent();
        }
    }
}