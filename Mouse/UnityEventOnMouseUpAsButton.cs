﻿using UnityEngine;

namespace Utilities.EventManagers.Mouse {
    class UnityEventOnMouseUpAsButton : UnityEventManager {
        void OnMouseUpAsButton() {
            InvokeEvent();
        }
    }
}