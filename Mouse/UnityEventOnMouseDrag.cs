﻿namespace Utilities.EventManagers.Mouse {
    class UnityEventOnMouseDrag : UnityEventManager {
        void OnMouseDrag() {
            InvokeEvent();
        }
    }
}