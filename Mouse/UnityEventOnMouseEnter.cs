﻿namespace Utilities.EventManagers {
    class UnityEventOnMouseEnter : UnityEventManager {
        void OnMouseEnter() {
            InvokeEvent();
        }
    }
}
