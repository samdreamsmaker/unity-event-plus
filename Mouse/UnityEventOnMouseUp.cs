﻿using UnityEngine;

namespace Utilities.EventManagers.Mouse {
    class UnityEventOnMouseUp : UnityEventManager {
        void OnMouseUp() {
            InvokeEvent();
        }
    }
}