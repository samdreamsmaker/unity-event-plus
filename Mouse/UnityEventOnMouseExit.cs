﻿namespace Utilities.EventManagers {
    class UnityEventOnMouseExit : UnityEventManager {
        void OnMouseExit() {
            InvokeEvent();
        }
    }
}
