﻿using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventReset : UnityEventManager {
        void Reset() {
            InvokeEvent();
        }
    }
}