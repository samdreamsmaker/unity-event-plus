﻿
using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventInvokeRepeating : UnityEventManager {
        [SerializeField]
        float delayTime;
        [SerializeField]
        float frequency = 1;

        void Start() {
            InvokeRepeating(nameof(InvokeEvent),delayTime,frequency);
        }
    }
}
