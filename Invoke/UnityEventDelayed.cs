﻿using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventDelayed : UnityEventManager {
        [SerializeField]
        float delayedTime;
        void Start() {
            Invoke(nameof(InvokeEvent),delayedTime);
        }
    }
}
