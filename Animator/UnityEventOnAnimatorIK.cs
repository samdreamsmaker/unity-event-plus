﻿using UnityEngine;

namespace Utilities.EventManagers.Animator {
    class UnityEventOnAnimatorIK : UnityEventManager {
        void OnAnimatorIK() {
            InvokeEvent();
        }
    }
}