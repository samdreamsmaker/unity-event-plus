﻿using UnityEngine;

namespace Utilities.EventManagers.Animator {
    class UnityEventOnAnimatorMove : UnityEventManager {
        void OnAnimatorMove() {
            InvokeEvent();
        }
    }
}