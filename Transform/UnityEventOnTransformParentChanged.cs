﻿using UnityEngine;

namespace Utilities.EventManagers.Transform {
    class UnityEventOnTransformParentChanged : UnityEventManager {
        void OnTransformParentChanged() {
            InvokeEvent();
        }
    }
}