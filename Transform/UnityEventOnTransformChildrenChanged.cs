﻿using UnityEngine;

namespace Utilities.EventManagers.Transform {
    class UnityEventOnTransformChildrenChanged : UnityEventManager {
        void OnTransformChildrenChanged() {
            InvokeEvent();
        }
    }
}