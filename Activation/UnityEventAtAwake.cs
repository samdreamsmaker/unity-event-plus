﻿namespace Utilities.EventManagers {
    class UnityEventAtAwake : UnityEventManager {
        void Awake() {
            InvokeEvent();
        }
    }
}