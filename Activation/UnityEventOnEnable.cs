﻿using UnityEngine;

namespace Utilities.EventManagers.Activation {
    class UnityEventOnEnable : UnityEventManager {
        void OnEnable() {
            InvokeEvent();
        }
    }
}