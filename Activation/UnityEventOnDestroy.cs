﻿namespace Utilities.EventManagers {
    class UnityEventOnDestroy : UnityEventManager {
        void OnDestroy() {
            InvokeEvent();
        }
    }
}