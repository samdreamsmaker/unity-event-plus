﻿using UnityEngine;

namespace Utilities.EventManagers.Activation {
    class UnityEventOnDisable : UnityEventManager {
        void OnDisable() {
            InvokeEvent();
        }
    }
}