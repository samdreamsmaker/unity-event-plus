﻿using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventOnValidate : UnityEventManager {
        void OnValidate() {
            InvokeEvent();
        }
    }
}