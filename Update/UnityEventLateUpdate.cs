﻿using UnityEngine;

namespace Utilities.EventManagers {
    class UnityEventLateUpdate : UnityEventManager {
        void LateUpdate() {
            InvokeEvent();
        }
    }
}