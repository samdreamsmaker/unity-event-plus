﻿namespace Utilities.EventManagers {
    class UnityEventFixedUpdate : UnityEventManager {
        void FixedUpdate() {
            InvokeEvent();
        }
    }
}
