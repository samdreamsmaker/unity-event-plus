﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnRenderImage : UnityEventManager {
        void OnRenderImage(RenderTexture src, RenderTexture dest) {
            InvokeEvent();
        }
    }
}