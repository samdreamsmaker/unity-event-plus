﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnRenderObject : UnityEventManager {
        void OnRenderObject() {
            InvokeEvent();
        }
    }
}