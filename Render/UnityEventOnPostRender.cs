﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnPostRender : UnityEventManager {
        void OnPostRender() {
            InvokeEvent();
        }
    }
}