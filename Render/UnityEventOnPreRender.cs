﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnPreRender : UnityEventManager {
        void OnPreRender() {
            InvokeEvent();
        }
    }
}