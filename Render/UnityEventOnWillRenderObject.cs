﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnWillRenderObject : UnityEventManager {
        void OnWillRenderObject() {
            InvokeEvent();
        }
    }
}