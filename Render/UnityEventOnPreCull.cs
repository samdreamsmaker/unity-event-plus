﻿using UnityEngine;

namespace Utilities.EventManagers.Render {
    class UnityEventOnPreCull : UnityEventManager {
        void OnPreCull() {
            InvokeEvent();
        }
    }
}