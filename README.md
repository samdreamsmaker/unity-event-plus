# Unity Event Plus

Unity Event Plus is a plugin to simplify your code.

- clean source code
- simple concepts
- beginner-friendly
- optimization
- reduce source code quantity


With Unity Event Plus you will stop to rewrite all functions of Unity like Awake, Start, OnDestroy etc. 
Your code isn't locked after the compilation. 
Add components & events dynamically to generate unpredictable behavior. 
Useful for strong artificial intelligences. 
For the masters of design patterns, it's an amazing merge of patterns Event Aggregator, Delegation & Monobehaviour. 